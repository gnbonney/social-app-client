import React from 'react';
import {connect} from 'react-redux';
import {Card, Container, Dropdown, Grid, Menu, Responsive} from 'semantic-ui-react'
import axios from "axios";
import {authHeader} from "../helpers";
import {Link} from "react-router-dom";
import MarkdownPreview from "../markdown/MarkdownPreview";
import moment from 'moment';

const getWidth = () => {
  const isSSR = typeof window === 'undefined';

  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth;
};

// TODO: in tutorial config.apiUrl was set in webpack.config.js, but that's because they ejected
// FIXME: figure out how to set apiUrl in env
const config = {apiUrl: 'http://192.168.1.129:4000'}

class MemberPage extends React.Component {
  constructor(props) {
    super(props);
    const {match} = props
    console.log(`handle=${match.params.handle}`)
    this.state = {activeItem: 'tumblelog', member:{}};
  }

  componentDidMount() {
    this.getPostsList();
    this.getProfile();
  }

  getProfile() {
    const {match} = this.props
    axios.get(`${config.apiUrl}/users/byName/${match.params.handle}`, {headers: authHeader()})
      .then((response) => {
        // handle success
        this.setState({member: response.data})
      })
      .catch((error) => {
        // handle error
        console.log(error)
      })
      .then(() => {
        // always executed
      });
    axios.get(`${config.apiUrl}/pages/${match.params.handle}`, {headers: authHeader()})
      .then((response) => {
        // handle success
        this.setState({page: response.data.markdown})
      })
      .catch((error) => {
        // handle error
        console.log(error)
      })
      .then(() => {
        // always executed
      });
  }

  getPostsList() {
    const {match} = this.props
    axios.get(`${config.apiUrl}/tumblelogs/posts/${match.params.handle}`, {headers: authHeader()})
      .then((response) => {
        // handle success
        this.setState({list: response.data})
        this.getBlogPost();
      })
      .catch((error) => {
        // handle error
        console.log(error)
      })
      .then(() => {
        // always executed
      });
  }

  getBlogPost(date) {
    const {match} = this.props
    const {list} = this.state
    axios.get(`${config.apiUrl}/tumblelogs/post/${match.params.handle}/${date?date:moment().format('YYYY-MM-DD')}`, {headers: authHeader()})
      .then((response) => {
        // handle success
        this.setState({post: response.data.markdown})
      })
      .catch((error) => {
        // handle error
        if (list && (list.length > 0)) {
          axios.get(`${config.apiUrl}/tumblelogs/post/${match.params.handle}/${list[0]}`, {headers: authHeader()})
            .then((response) => {
              this.setState({post: response.data.markdown})
            })
            .catch((error) => {
              console.log(error)
            })
        }
      })
      .then(() => {
        // always executed
      });
  }

  handleItemClick = (e, {name}) => {
    this.setState({activeItem: name})
  }

  handleChange = (txt) => {
    this.setState({post: txt})
  }

  ActionsSideMenu = () => {
    return <Dropdown item icon='content'>
      <Dropdown.Menu>
        <this.PostsMenu/>
      </Dropdown.Menu>
    </Dropdown>
  }

  PostsList = () => (this.state.list ?
      this.state.list.sort().reverse().map(date => (<p><Link onClick={() => (this.getBlogPost(date))} to={`/${this.props.match.params.handle}/${date}`}>{date}</Link></p>))
      : null
  )

  PostsMenu = () => (this.state.list ?
      this.state.list.sort().reverse().map(date => (<Dropdown.Item><Link onClick={() => (this.getBlogPost(date))} to={`/${this.props.match.params.handle}/${date}`}>{date}</Link></Dropdown.Item>))
      : null
  )

  DesktopLayout = (activeItem, post, page) => (
    <Responsive getWidth={getWidth} minWidth={Responsive.onlyTablet.minWidth}>
      <Container fluid>
        <Menu>
          <Menu.Item
            name='tumblelog'
            active={activeItem === 'tumblelog'}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            name='page'
            active={activeItem === 'page'}
            onClick={this.handleItemClick}
          />
        </Menu>
        {activeItem === 'tumblelog' ?
          <Grid columns={3}>
            <Grid.Row>
              <Grid.Column width={4}>
                <Container>
                  <Card>
                    <Card.Content>
                      <Card.Header>{this.state.member.nickname}</Card.Header>
                      <Card.Meta>Member since {moment(this.state.member.createdAt).format('MMMM Do YYYY')}</Card.Meta>
                    </Card.Content>
                    <Card.Content extra>
                      <Link to={"/"+this.state.member.username}>@{this.state.member.username}</Link>
                    </Card.Content>
                  </Card>
                </Container>
              </Grid.Column>
              <Grid.Column width={8}>
                <MarkdownPreview
                  value={post && post.length > 0 ? post : "Nothing posted for this day."}
                  className="column comment-preview"/>
              </Grid.Column>
              <Grid.Column width={4}>
                <this.PostsList/>
              </Grid.Column>
            </Grid.Row>
          </Grid>
          : null
        }
        {activeItem === 'page' ?
          <Grid columns={3}>
            <Grid.Row>
              <Grid.Column width={4}>
                <Container>
                  <Card>
                    <Card.Content>
                      <Card.Header>{this.state.member.nickname}</Card.Header>
                      <Card.Meta>Member since {moment(this.state.member.createdAt).format('MMMM Do YYYY')}</Card.Meta>
                    </Card.Content>
                    <Card.Content extra>
                      <Link to={"/"+this.state.member.username}>@{this.state.member.username}</Link>
                    </Card.Content>
                  </Card>
                </Container>
              </Grid.Column>
              <Grid.Column width={8}>
                <MarkdownPreview
                  value={page && page.length > 0 ? page : "Nothing found."}
                  className="column comment-preview"/>
              </Grid.Column>
              <Grid.Column width={4}></Grid.Column>
            </Grid.Row>
          </Grid>
          : null
        }
      </Container>
    </Responsive>
  )

  MobileLayout = (activeItem, post, page) => (
    <Responsive getWidth={getWidth} maxWidth={Responsive.onlyMobile.maxWidth}
    >
      <div>
        <Menu>
          <Menu.Item
            name='tumblelog'
            active={activeItem === 'tumblelog'}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            name='edit'
            active={activeItem === 'edit'}
            onClick={this.handleItemClick}
          />
          <Menu.Menu position='right'>
            <this.ActionsSideMenu/>
          </Menu.Menu>
        </Menu>
        <Container>
          <Card>
            <Card.Content>
              <Card.Header>{this.state.member.nickname}</Card.Header>
              <Card.Meta>Member since {moment(this.state.member.createdAt).format('MMMM Do YYYY')}</Card.Meta>
            </Card.Content>
            <Card.Content extra>
              <Link to={"/"+this.state.member.username}>@{this.state.member.username}</Link>
            </Card.Content>
          </Card>
        </Container>
        {activeItem === 'tumblelog' ?
          <Container>
            <MarkdownPreview
              value={post && post.length > 0 ? post : "Nothing posted for this day."}
              className="column comment-preview"/>
          </Container>
          : null
        }
        {activeItem === 'edit' ?
          <Container>
            <MarkdownPreview
              value={page && page.length > 0 ? page : "Nothing found."}
              className="column comment-preview"/>
          </Container>
          : null
        }
      </div>
    </Responsive>
  )

  render() {
    const {activeItem, post, page} = this.state
    return (
      <div style={{padding:'0px'}}>
        {this.DesktopLayout(activeItem, post, page)}
        {this.MobileLayout(activeItem, post, page)}
      </div>
    );
  }
}

function mapState(state) {
  const {authentication} = state;
  const {user} = authentication;
  return {user};
}

const connectedMemberPage = connect(mapState)(MemberPage);
export {connectedMemberPage as MemberPage};
