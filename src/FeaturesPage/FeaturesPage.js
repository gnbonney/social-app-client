import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {Card, Container, Header} from 'semantic-ui-react'
import Features from "./Features";

import {userActions} from '../actions';

class FeaturesPage extends React.Component {
  componentDidMount() {
    this.props.getUsers();
  }

  render() {
    const {users} = this.props;
    return (
      <Container>
        <Features/>
      </Container>
    );
  }
}

function mapState(state) {
  const {users, authentication} = state;
  const {user} = authentication;
  return {user, users};
}

const actionCreators = {
  getUsers: userActions.getAll,
  deleteUser: userActions.delete
}

const connectedFeaturesPage = connect(mapState, actionCreators)(FeaturesPage);
export {connectedFeaturesPage as FeaturesPage};
