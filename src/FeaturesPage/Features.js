import { Grid, Header, Icon, List } from 'semantic-ui-react'
import React from "react";

const Features = () => {
  return <Grid celled="internally" columns="equal" stackable>
    <Grid.Row textAlign="center">
      <Grid.Column style={{ paddingBottom: '5em', paddingTop: '5em' }}>
        <Header as="h3" style={{ fontSize: '2em' }}>
          Slowing the spread of fake news
        </Header>
        <p style={{ fontSize: '1.33em' }}>
          <div>
            The like and share buttons on Facebook contribute to the proliferation of fake news, conspiracy theories, and political memes
            on their platform. This flaw has been exploited by governments seeking to influence elections in other countries.
            Instead of fixing the problem, they have pushed responsibility off onto third party fact checkers.
          </div>
          <div>
            We should not pretend that an army of fact-checkers could ever keep up with and police everything people say on social media.
            Nor should we trust A.I. to make those decisions for us.
          </div>
          <div>
            <b>WikiTumble does not have the like and share buttons that cause fake news to go viral.</b>
          </div>
        </p>
      </Grid.Column>
      <Grid.Column style={{ paddingBottom: '5em', paddingTop: '5em' }}>
        <Header as="h3" style={{ fontSize: '2em' }}>
          Pumping the brakes on viral public shaming
        </Header>
        <p style={{ fontSize: '1.33em' }}>
          <div>
            Since 2015 many have raised the concern about public shaming on Twitter, but
            according to Debbie Chachra, professor at Olin College of Engineering,
            Twitter's harassment problem is baked into its design.
            It is simply too easy for people to "retweet" without understanding the original context
            or background of the person they are judging.
          </div>
          <div>
            <b>WikiTumble does not have an equivalent feature that would cause public shaming to go viral.</b>
          </div>
        </p>
      </Grid.Column>
      <Grid.Column style={{ paddingBottom: '5em', paddingTop: '5em' }}>
        <Header as="h3" style={{ fontSize: '2em' }}>
          No psychological exploitation
        </Header>
        <p style={{ fontSize: '1.33em' }}>
          <p>
            Sean Parker, co-founder of Facebook, has been quoted as saying
            "It's a social-validation feedback loop ... exactly the kind of thing that
            a hacker like myself would come up with, because you're exploiting a
            vulnerability in human psychology."
            Parker also said, "God only knows what it's doing to our children's brains."
          </p>
          <p>
            <b>WikiTumble breaks that unhealthy feedback loop by not providing reaction buttons.</b>
          </p>
        </p>
      </Grid.Column>
    </Grid.Row>
    <Grid.Row textAlign="center">
      <Grid.Column style={{ paddingBottom: '5em', paddingTop: '5em' }}>
        <Header as="h3" style={{ fontSize: '2em' }}>
          Promoting positivity
        </Header>
        <p style={{ fontSize: '1.33em' }}>
          <div>
            Tumblr is plagued by negativity.
            One user wrote, “As I used Tumblr more and more and followed more blogs, I slowly became
            more exposed to not only art and creativity, but to negativity.”
            Tumblr attempted to make "a better, more positive" place by banning NSFW - not safe for work - media.
          </div>
          <div>
            <b>WikiTumble does not host images and linking to NSFW (not safe for work) images is prohibited.</b>
          </div>
          <div>
            Future feature: We do not wish to contribute to depression and anxiety.
            We are working on a feature to use sentiment analysis to rate the positivity of each
            tumblelog post and then roll that up to an overall rating for each tumblelog.
            Members could then make an informed decision if they want to avoid tumblelogs that tend to be
            particularly negative.
          </div>
        </p>
      </Grid.Column>
      <Grid.Column style={{ paddingBottom: '5em', paddingTop: '5em' }}>
        <Header as="h3" style={{ fontSize: '2em' }}>
          No legal name discrimination
        </Header>
        <p style={{ fontSize: '1.33em' }}>
          <div>
            Facebook requires use of legal names. Members of some minority groups have felt
            targeted because of their real/legal names. Many other people would just prefer not
            to use their real/legal name for privacy or online safety reasons.
          </div>
          <div>
            <b>
              WikiTumble does not require or ask for real/legal name.
            </b>
          </div>
          <div>
            (If you want to use any portion of your real name in your nickname that is up to you.)
          </div>
        </p>
      </Grid.Column>
      <Grid.Column style={{ paddingBottom: '5em', paddingTop: '5em' }}>
        <Header as="h3" style={{ fontSize: '2em' }}>
          Free Verification for Qualified Users
        </Header>
        <p style={{ fontSize: '1.33em' }}>
          <p>
            When Elon Musk took over Twitter he attempted to monetize their verified user feature.
            Novelist Stephen King threatened to leave the platform if he had to pay for verified status.
            Comedian Kathy Griffith trolled Musk by changing her verified account to impersonate him.
          </p>
          <p>
            <b>
              WikiTumble automatically verifies certain categories of users by their official e-mail
              address. We do not verify identity.
            </b>
          </p>
          <p>
            To be considered for verified status, register with an official e-mail address. It will be kept private.
            (Disposable e-mails are always blocked.)
          </p>
          <List>
            <List.Item>
              <Icon name="newspaper" />
              &nbsp;A valid press (newspaper or other news organization) email address
            </List.Item>
            <List.Item>
              <Icon name="law" />
              &nbsp;A government email address
            </List.Item>
            <List.Item>
              <Icon name="university" />
              &nbsp;A valid college or university email address
            </List.Item>
            <List.Item>
              <Icon name="industry" />
              &nbsp;A valid fortune 500 email address.
            </List.Item>
          </List>
        </p>
      </Grid.Column>
    </Grid.Row>
  </Grid>
}

export default Features
