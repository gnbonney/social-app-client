import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {Button, Form, Grid} from 'semantic-ui-react'

import { userActions } from '../actions';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        // reset login status
        this.props.logout();

        this.state = {
            username: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password } = this.state;
        if (username && password) {
            this.props.login(username, password);
        }
    }

    render() {
        const { loggingIn } = this.props;
        const { username, password, submitted } = this.state;
        return (
          <Grid columns={3}>
              <Grid.Column width={4}></Grid.Column>
              <Grid.Column width={8}>
                  <h2>Login</h2>
                  <Form onSubmit={this.handleSubmit}>
                      {/*<div className={'field' + (submitted && !username ? ' error' : '')}>*/}
                      <div className={'field' + (submitted && !username ? ' error' : '')}>
                          <label htmlFor="username">Username</label>
                          <input type="text" name="username" value={username} onChange={this.handleChange} />
                          {submitted && !username &&
                          <div className="ui message">Username is required</div>
                          }
                      </div>
                      <div className={'field' + (submitted && !password ? ' error' : '')}>
                          <label htmlFor="password">Password</label>
                          <input type="password" name="password" value={password} onChange={this.handleChange} />
                          {submitted && !password &&
                          <div className="ui message">Password is required</div>
                          }
                      </div>
                      <div className="field">
                          {loggingIn ? <Button loading>Login</Button>
                            : <Button>Login</Button>}
                          <Link to="/register">Register</Link>
                      </div>
                  </Form>
              </Grid.Column>
          </Grid>
        );
    }
}

function mapState(state) {
    const { loggingIn } = state.authentication;
    return { loggingIn };
}

const actionCreators = {
    login: userActions.login,
    logout: userActions.logout
};

const connectedLoginPage = connect(mapState, actionCreators)(LoginPage);
export { connectedLoginPage as LoginPage };
