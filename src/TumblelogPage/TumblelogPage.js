import React from 'react';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import axios from 'axios';
import {LiveMarkdownTextarea} from '../markdown';
import {Button, Checkbox, Container, Dropdown, Form, Grid, Icon, Menu, Modal, Responsive, TextArea} from 'semantic-ui-react'
import {authHeader} from '../helpers/auth-header';
import MarkdownPreview from "../markdown/MarkdownPreview";
import moment from 'moment';

const getWidth = () => {
  const isSSR = typeof window === 'undefined';

  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth;
};

// TODO: in tutorial config.apiUrl was set in webpack.config.js, but that's because they ejected
// FIXME: figure out how to set apiUrl in env
const config = {apiUrl: 'http://192.168.1.129:4000'}

class TumblelogPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {activeItem: 'tumblelog', thoughtOpen: false, menuVisible: false, checked: true, hashtags: ''};
  }

  componentDidMount() {
    this.getBlogPost();
    this.getPostsList();
  }

  getPostsList() {
    const {user} = this.props
    axios.get(`${config.apiUrl}/tumblelogs/posts/${user.username}`, {headers: authHeader()})
      .then((response) => {
        // handle success
        this.setState({list: response.data})
      })
      .catch((error) => {
        // handle error
        console.log(error)
      })
      .then(() => {
        // always executed
      });
  }

  getBlogPost(newProps) {
    const {user, match} = newProps ? newProps : this.props
    axios.get(`${config.apiUrl}/tumblelogs/post/${user.username}/${match.params.date}`, {headers: authHeader()})
      .then((response) => {
        // handle success
        this.setState({id: response.data.id, value: response.data.markdown})
      })
      .catch((error) => {
        // handle error
        console.log(error)
      })
      .then(() => {
        // always executed
      });
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    this.getBlogPost(newProps);
  }

  handleItemClick = (e, {name}) => {
    if (name === 'edit') {
      this.setState({temp: this.state.value})
    }
    this.setState({activeItem: name})
  }

  cancelClicked = (event) => {
    this.setState({value: this.state.temp, activeItem: 'tumblelog'})
    // cancel the event to prevent form submit
    event.preventDefault();
  }

  publishClicked = (event) => {
    const {value} = this.state
    this.save(value);
    // cancel the event to prevent the default submit behavior
    if (event) {
      event.preventDefault();
    }
  }

  save(value) {
    const {user, match} = this.props
    const {id} = this.state
    axios.post(`${config.apiUrl}/tumblelogs/upsert`, {
      id,
      name: user.username,
      posted: match.params.date ? match.params.date : moment().format('YYYY-MM-DD'),
      markdown: value,
      createdBy: user.username
    }, {headers: authHeader()})
      .then((response) => {
        // handle success
        this.setState({id: response.data.id, activeItem: 'tumblelog'})
      })
      .catch((error) => {
        // handle error
        console.log(error);
        // TODO: display some error
      })
      .then(() => {
        // always executed
      });
  }

  handleChange = (txt) => {
    this.setState({value: txt})
  }

  handleModalChange = (e) => {
    const {name, value} = e.target;
    this.setState({[name]: value});
  }

  parseHashtags = (str) => {
    const regexp = /#[a-zA-Z][0-9a-zA-Z_]+/g
    return [...str.matchAll(regexp)]
  }

  ThoughtButton = (param) => (
    <Modal size='small'
           trigger={ param.trigger }
           open={this.state.thoughtOpen}
           onClose={() => (this.setState({thoughtOpen: false, menuVisible: false}))}
    >
      <Modal.Content>
        <Form>
          <Form.Field>
            <TextArea
              placeholder='Enter your thoughts here'
              name='thought'
              value={this.state.thought}
              onChange={this.handleModalChange}
            />
          </Form.Field>
          <Form.Field>
            <input
              placeholder='hashtags'
              name='hashtags'
              value={this.state.hashtags}
              onChange={this.handleModalChange}
            />
          </Form.Field>
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' inverted
                onClick={() => (this.setState({thoughtOpen: false, menuVisible: false}))}
        >
          <Icon name='remove'/> Cancel
        </Button>
        <Button color='green' inverted
                onClick={() => {
                  const hashtags = this.parseHashtags(this.state.hashtags).join(' ')
                  const hashtagBlock = hashtags.length ? `\n<p>${hashtags}</p>` : ''
                  const thoughtStr = `<div class="thought"><span>Thought</span>${this.state.thought}${hashtagBlock}</div>\n\n`
                  const temp = thoughtStr + (this.state.value ? this.state.value : "");
                  this.save(temp);
                  this.setState({value: temp, thought: null, thoughtOpen: false, menuVisible: false, hashtags: ''});
                }}
        >
          <Icon name='checkmark'/> Add
        </Button>
      </Modal.Actions>
    </Modal>
  )

  QuoteButton = (param) => (
    <Modal size='small'
           trigger={ param.trigger }
           open={this.state.quoteOpen}
           onClose={() => (this.setState({quoteOpen: false}))}
    >
      <Modal.Content>
        <Form>
          <Form.Field>
            <TextArea
              placeholder='Enter quote here'
              name='quote'
              value={this.state.quote}
              onChange={this.handleModalChange}
            />
          </Form.Field>
          <Form.Field>
            <input
              placeholder='Who said it?'
              name='cite'
              value={this.state.cite}
              onChange={this.handleModalChange}
            />
            {this.state.quoteLoading ? <Button loading>Search by name</Button> :
              <Button onClick={() => {
                this.setState({quoteLoading: true})
                axios.get(`${config.apiUrl}/tumblelogs/quote/${this.state.cite}`, {headers: authHeader()})
                  .then((response) => {
                    // handle success
                    this.setState({quote: response.data.quote})
                  })
                  .catch((error) => {
                    // handle error
                    console.log(error)
                  })
                  .then(() => {
                    this.setState({quoteLoading: false})
                  });
              }}>Search by name</Button>
            }
            {this.state.fortuneLoading ? <Button loading>Random Fortune</Button> :
              <Button onClick={() => {
                this.setState({fortuneLoading: true})
                axios.get(`${config.apiUrl}/tumblelogs/fortune`, {headers: authHeader()})
                  .then((response) => {
                    let fortune = response.data.fortune
                    console.log("fortune="+fortune)
                    console.log("fortune.length="+fortune.length)
                    const startOfLastLine = fortune.substring(0,fortune.length-1).lastIndexOf("\n")
                    console.log("startOfLastLine="+startOfLastLine)
                    // handle success
                    if(startOfLastLine>0) {
                      const lastLine = fortune.substring(startOfLastLine + 1).trim();
                      console.log("lastLine=" + lastLine)
                      if (lastLine.startsWith("--")) {
                        this.setState({cite: lastLine.substring(2, lastLine.length).trim()})
                        fortune = fortune.substring(0, startOfLastLine)
                      } else if (lastLine.startsWith("-")) {
                        this.setState({cite: lastLine.substring(1, lastLine.length).trim()})
                        fortune = fortune.substring(0, startOfLastLine)
                      } else if (lastLine.startsWith("~")) {
                        this.setState({cite: lastLine.substring(1, lastLine.length).trim()})
                        fortune = fortune.substring(0, startOfLastLine)
                      } else {
                        this.setState({cite: ''})
                      }
                    } else {
                      this.setState({cite: ''})
                    }
                    this.setState({quote: fortune })
                  })
                  .catch((error) => {
                    // handle error
                    console.log(error)
                  })
                  .then(() => {
                    this.setState({fortuneLoading: false})
                  });
              }}>Random Fortune</Button>
            }
          </Form.Field>
          <Form.Field>
            <input
              placeholder='hashtags'
              name='hashtags'
              value={this.state.hashtags}
              onChange={this.handleModalChange}
            />
          </Form.Field>
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' inverted
                onClick={() => (this.setState({quoteOpen: false}))}
        >
          <Icon name='remove'/> Cancel
        </Button>
        <Button color='green' inverted
                onClick={() => {
                  const {cite} = this.state
                  const str = this.state.quote;
                  const footer = cite ? `<footer><cite>&mdash; ${cite}</cite></footer>` : "";
                  const hashtags = this.parseHashtags(this.state.hashtags).join(' ')
                  const hashtagBlock = hashtags.length ? `\n<p>${hashtags}</p>` : ''
                  const quoteStr = `<blockquote class="style1">${str}${footer}${hashtagBlock}</blockquote>\n\n`;
                  const temp = quoteStr + (this.state.value ? this.state.value : "");
                  this.save(temp);
                  this.setState({value: temp, quote: null, cite: null, quoteOpen: false, hashtags:''});
                }}
        >
          <Icon name='checkmark'/> Add
        </Button>
      </Modal.Actions>
    </Modal>
  )

  toggle = () => this.setState((prevState) => ({ checked: !prevState.checked }))
  LyricsButton = (param) => (
    <Modal size='small'
           trigger={ param.trigger }
           open={this.state.lyricsOpen}
           onClose={() => (this.setState({lyricsOpen: false}))}
    >
      <Modal.Content>
        <Form>
          <Form.Field>
            <TextArea
              placeholder='Enter lyrics here'
              name='lyrics'
              value={this.state.lyrics}
              onChange={this.handleModalChange}
            />
          </Form.Field>
          <Form.Field>
            <input
              placeholder='song title'
              name='song'
              value={this.state.song}
              onChange={this.handleModalChange}
            />
            by
            <input
              placeholder='artist'
              name='artist'
              value={this.state.artist}
              onChange={this.handleModalChange}
            />
            <p>
              <Checkbox label='first verse only' defaultChecked
                        onChange={this.toggle}
                        checked={this.state.checked}
              />
            </p>
            {this.state.lyricsLoading ? <Button loading>Search by song & artist</Button> :
              <Button onClick={() => {
                const {artist, song} = this.state;
                this.setState({lyricsLoading: true})
                if (artist && song) {
                  axios.get(`${config.apiUrl}/tumblelogs/lyrics/${artist}/${song}`, {headers: authHeader()})
                    .then((response) => {
                      // handle success
                      const result = this.state.checked ? response.data.lyrics.split("\n\n", 1)[0] : response.data.lyrics;
                      this.setState({lyrics: result})
                    })
                    .catch((error) => {
                      // handle error
                      console.log(error)
                    })
                    .then(() => {
                      this.setState({lyricsLoading: false})
                    });
                } else {
                  this.setState({lyricsLoading: false})
                }
              }}>Search by song & artist</Button>
            }
          </Form.Field>
          <Form.Field>
            <input
              placeholder='hashtags'
              name='hashtags'
              value={this.state.hashtags}
              onChange={this.handleModalChange}
            />
          </Form.Field>
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' inverted
                onClick={() => (this.setState({lyricsOpen: false}))}
        >
          <Icon name='remove'/> Cancel
        </Button>
        <Button color='green' inverted
                onClick={() => {
                  const {artist, song} = this.state
                  const str = this.state.lyrics
                  const footer = song ? `<footer><cite>&mdash; ${song}${artist ? " by "+artist : ""}</cite></footer>` : "";
                  const hashtags = this.parseHashtags(this.state.hashtags).join(' ')
                  const hashtagBlock = hashtags.length ? `\n<p>${hashtags}</p>` : ''
                  const lyricsStr = `<blockquote class="lyrics">${str}${footer}${hashtagBlock}</blockquote>\n\n`;
                  const temp = lyricsStr + (this.state.value ? this.state.value : "");
                  this.save(temp);
                  this.setState({value: temp, lyrics: null, cite: null, lyricsOpen: false, hashtags: ''});
                }}
        >
          <Icon name='checkmark'/> Add
        </Button>
      </Modal.Actions>
    </Modal>
  )

  CodeButton = (param) => (
    <Modal size='small'
           trigger={ param.trigger }
           open={this.state.codeOpen}
           onClose={() => (this.setState({codeOpen: false}))}
    >
      <Modal.Content>
        <Form>
          <Form.Field>
            <TextArea
              placeholder='Enter code here'
              name='code'
              value={this.state.code}
              onChange={this.handleModalChange}
            />
          </Form.Field>
          <Form.Field>
            <input
              placeholder='language'
              name='language'
              value={this.state.language}
              onChange={this.handleModalChange}
            />
          </Form.Field>
          <Form.Field>
            <input
              placeholder='hashtags'
              name='hashtags'
              value={this.state.hashtags}
              onChange={this.handleModalChange}
            />
          </Form.Field>
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' inverted
                onClick={() => (this.setState({codeOpen: false}))}
        >
          <Icon name='remove'/> Cancel
        </Button>
        <Button color='green' inverted
                onClick={() => {
                  const {code, language} = this.state
                  const hashtags = this.parseHashtags(this.state.hashtags).join(' ')
                  const codeStr = `\`\`\`${language}\n${code}\n\`\`\`\n${hashtags}\n\n`;
                  const temp = codeStr + (this.state.value ? this.state.value : "");
                  this.save(temp);
                  this.setState({value: temp, code: null, codeOpen: false, hashtags: ''});
                }}
        >
          <Icon name='checkmark'/> Add
        </Button>
      </Modal.Actions>
    </Modal>
  )

  LinkButton = (param) => (
    <Modal size='small'
           trigger={ param.trigger }
           open={this.state.linkOpen}
           onClose={() => (this.setState({linkOpen: false}))}
    >
      <Modal.Content>
        <Form>
          <Form.Field>
            <input
              placeholder='Enter url here'
              name='href'
              value={this.state.href}
              onChange={this.handleModalChange}
            />
          </Form.Field>
          <Form.Field>
            <input
              placeholder='Enter link text here'
              name='linktext'
              value={this.state.linktext}
              onChange={this.handleModalChange}
            />
          </Form.Field>
          <Form.Field>
            <TextArea
              placeholder='Enter comment/description here'
              name='desc'
              value={this.state.desc}
              onChange={this.handleModalChange}
            />
          </Form.Field>
          <Form.Field>
            <input
              placeholder='hashtags'
              name='hashtags'
              value={this.state.hashtags}
              onChange={this.handleModalChange}
            />
          </Form.Field>
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' inverted
                onClick={() => (this.setState({linkOpen: false}))}
        >
          <Icon name='remove'/> Cancel
        </Button>
        <Button color='green' inverted
                onClick={() => {
                  const {href, linktext, desc} = this.state
                  const hashtags = this.parseHashtags(this.state.hashtags).join(' ')
                  axios.post(`${config.apiUrl}/pages/preview`, {url: href}, {headers: authHeader()})
                    .then((response) => {
                      const linkStr =
                        `<p class="link">
  <a href="${href}">${linktext ? linktext : response.data.title}</a> ${desc ? desc : ''}</p>\n<div class="ui card">
  <div class="image">
    <img src=${response.data.img}>
  </div>
  <div class="content">
    <a class="header" href=${href}>${response.data.title}</a>
    <div class="meta">
      <span class="date">${response.data.domain}</span>
    </div>
    <div class="description">
      ${response.data.description} ${hashtags}
</div>
</div>
</div>\n\n`
                      const temp = linkStr + (this.state.value ? this.state.value : "");
                      this.save(temp);
                      this.setState({value: temp, href: null, linktext: null, desc: null, linkOpen: false, hashtags: ''});
                    })
                    .catch((error) => {
                      // handle error
                      console.log(error)
                      const hashtags = this.parseHashtags(this.state.hashtags).join(' ')
                      const linkStr = `[${linktext ? linktext : href}](${href}) ${hashtags}\n\n`
                      const temp = linkStr + (this.state.value ? this.state.value : "");
                      this.save(temp);
                      this.setState({value: temp, href: null, linktext: null, desc: null, linkOpen: false, hashtags: ''});
                    })

                }}
        >
          <Icon name='checkmark'/> Add
        </Button>
      </Modal.Actions>
    </Modal>
  )

  ImageButton = (param) => (
    <Modal size='small'
           trigger={ param.trigger }
           open={this.state.imageOpen}
           onClose={() => (this.setState({imageOpen: false}))}
    >
      <Modal.Content>
        <Form>
          <Form.Field>
            <input
              placeholder='Enter url here'
              name='href'
              value={this.state.href}
              onChange={this.handleModalChange}
            />
          </Form.Field>
          <Form.Field>
            <input
              placeholder='Enter image text here'
              name='imagetext'
              value={this.state.imagetext}
              onChange={this.handleModalChange}
            />
          </Form.Field>
          <Form.Field>
            <TextArea
              placeholder='Enter comment/description here'
              name='desc'
              value={this.state.desc}
              onChange={this.handleModalChange}
            />
          </Form.Field>
          <Form.Field>
            <input
              placeholder='hashtags'
              name='hashtags'
              value={this.state.hashtags}
              onChange={this.handleModalChange}
            />
          </Form.Field>
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' inverted
                onClick={() => (this.setState({imageOpen: false}))}
        >
          <Icon name='remove'/> Cancel
        </Button>
        <Button color='green' inverted
                onClick={() => {
                  const {href, imagetext, desc} = this.state
                  axios.post(`${config.apiUrl}/pages/preview`, {url: href}, {headers: authHeader()})
                    .then((response) => {
                      const hashtags = this.parseHashtags(this.state.hashtags).join(' ')
                      const imageStr =
                        `<p class="link">
  <a href="${href}">${imagetext ? imagetext : response.data.title}</a> ${desc ? desc : ''}</p>\n<div class="ui card">
  <div class="image">
    <img src=${response.data.img}>
  </div>
  <div class="content">
    <a class="header" href=${href}>${response.data.title}</a>
    <div class="meta">
      <span class="date">${response.data.domain}</span>
    </div>
    <div class="description">
      ${response.data.description ? response.data.description : response.data.domain} ${hashtags}
</div>
</div>
</div>\n\n`
                      const temp = imageStr + (this.state.value ? this.state.value : "");
                      this.save(temp);
                      this.setState({value: temp, href: null, imagetext: null, desc: null, imageOpen: false, hashtags: ''});
                    })
                    .catch((error) => {
                      // handle error
                      console.log(error)
                      const hashtags = this.parseHashtags(this.state.hashtags).join(' ')
                      const imageStr = `![${imagetext}](${href}) ${hashtags}\n\n`
                      const temp = imageStr + (this.state.value ? this.state.value : "");
                      this.save(temp);
                      this.setState({value: temp, href: null, imagetext: null, desc: null, imageOpen: false, hashtags: ''});
                    })

                }}
        >
          <Icon name='checkmark'/> Add
        </Button>
      </Modal.Actions>
    </Modal>
  )

  YoutubeButton = (param) => {
    return <Modal size='small'
           trigger={ param.trigger }
           open={this.state.youtubeOpen}
           onClose={() => (this.setState({youtubeOpen: false}))}
    >
      <Modal.Content>
        <Form>
          <Form.Field>
            <input
              placeholder='Enter url here'
              name='href'
              value={this.state.href}
              onChange={this.handleModalChange}
            />
          </Form.Field>
          <Form.Field>
            <input
              placeholder='Enter youtube text here'
              name='youtubetext'
              value={this.state.youtubetext}
              onChange={this.handleModalChange}
            />
          </Form.Field>
          <Form.Field>
            <TextArea
              placeholder='Enter comment/description here'
              name='desc'
              value={this.state.desc}
              onChange={this.handleModalChange}
            />
          </Form.Field>
          <Form.Field>
            <input
              placeholder='hashtags'
              name='hashtags'
              value={this.state.hashtags}
              onChange={this.handleModalChange}
            />
          </Form.Field>
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' inverted
                onClick={() => (this.setState({youtubeOpen: false}))}
        >
          <Icon name='remove'/> Cancel
        </Button>
        <Button color='green' inverted
                onClick={() => {
                  const {href, youtubetext, desc} = this.state
                  const hashtags = this.parseHashtags(this.state.hashtags).join(' ')
                  axios.post(`${config.apiUrl}/pages/preview`, {url: href}, {headers: authHeader()})
                    .then((response) => {
                      console.log('response='+JSON.stringify(response))
                      const youtubeStr =
                        `<p class="link">
  <a href="${href}">${youtubetext ? youtubetext : response.data.title}</a> ${desc ? desc : ''}</p>\n<div class="ui card">
  <div class="image">
    <img src=${response.data.img}>
  </div>
  <div class="content">
    <a class="header" href=${href}>${response.data.title}</a>
    <div class="meta">
      <span class="date">${response.data.domain}</span>
    </div>
    <div class="description">
      ${response.data.description} ${hashtags}
</div>
</div>
</div>\n\n`
                      const temp = youtubeStr + (this.state.value ? this.state.value : "");
                      this.save(temp);
                      this.setState({value: temp, href: null, youtubetext: null, desc: null, youtubeOpen: false, hashtags: ''});
                    })
                    .catch((error) => {
                      // handle error
                      console.log(error)
                      const hashtags = this.parseHashtags(this.state.hashtags).join(' ')
                      const youtubeStr = `<p class="youtube">
  <a href="${href}">${youtubetext}</a> ${desc ? desc : ''} ${hashtags}</p>\n\n`
                      const temp = youtubeStr + (this.state.value ? this.state.value : "");
                      this.save(temp);
                      this.setState({value: temp, href: null, youtubetext: null, desc: null, youtubeOpen: false, hashtags: ''});
                    })

                }}
        >
          <Icon name='checkmark'/> Add
        </Button>
      </Modal.Actions>
    </Modal>
  }

  ActionsMenu = () => {
    return <div>
      <Menu secondary stackable icon='labeled' size='tiny'>
        <this.ThoughtButton trigger={<Menu.Item name='thought' onClick={() => (this.setState({thoughtOpen: true}))}>
          <Icon name='lightbulb'/> Thought </Menu.Item>}/>

        {/* leverage https://www.npmjs.com/package/wikiquote */}
        {/* maybe also https://github.com/williamfligor/fortune */}
        <this.QuoteButton trigger={<Menu.Item name='quote' onClick={() => (this.setState({quoteOpen: true}))}>
          <Icon name='quote right'/> Quote </Menu.Item>}/>

        {/* leverage https://www.npmjs.com/package/lyrics-finder */}
        <this.LyricsButton trigger={<Menu.Item name='lyrics' onClick={() => (this.setState({lyricsOpen: true}))}>
          <Icon name='music'/> Lyrics </Menu.Item>}/>

        <this.CodeButton trigger={<Menu.Item name='code' onClick={() => (this.setState({codeOpen: true}))}>
          <Icon name='code'/> Code </Menu.Item>}/>

        {/* leverage https://github.com/AndrejGajdos/link-preview-generator */}
        <this.LinkButton trigger={<Menu.Item name='link' onClick={() => (this.setState({linkOpen: true}))}>
          <Icon name='linkify'/> Link </Menu.Item>}/>

        {/* TODO: leverage https://www.npmjs.com/package/react-giphy-searchbox */}
        {/* TODO: get giphy api key from https://developers.giphy.com/docs/sdk */}
        {/* TODO: leverage https://api.imgflip.com/ for memes, also see https://github.com/AnjaliSharma1234/Random-meme-generator */}
        <this.ImageButton trigger={<Menu.Item name='image' onClick={() => (this.setState({imageOpen: true}))}>
          <Icon name='picture'/> Image </Menu.Item>}/>

        {/* TODO: Initially this will just be manual copy/paste to avoid needing an api key */}
        <this.YoutubeButton trigger={<Menu.Item name='youtube' onClick={() => (this.setState({youtubeOpen: true}))}>
          <Icon name='youtube'/> Youtube </Menu.Item>}/>
      </Menu>
    </div>
  }

  ActionsSideMenu = () => {
    return <Dropdown item icon='content'>
        <Dropdown.Menu>
          <Dropdown.Item>
            <this.ThoughtButton trigger={<Link name='thought' onClick={() => (this.setState({thoughtOpen: true}))}>
              <Icon name='lightbulb'/> Thought </Link>}/>
          </Dropdown.Item>
          <Dropdown.Item>
            <this.QuoteButton trigger={<Link name='quote' onClick={() => (this.setState({quoteOpen: true}))}>
              <Icon name='quote right'/> Quote </Link>}/>
          </Dropdown.Item>
          <Dropdown.Item>
            <this.LyricsButton trigger={<Link name='lyrics' onClick={() => (this.setState({lyricsOpen: true}))}>
              <Icon name='music'/> Lyrics </Link>}/>
          </Dropdown.Item>
          <Dropdown.Item>
            <this.CodeButton trigger={<Link name='code' onClick={() => (this.setState({codeOpen: true}))}>
              <Icon name='code'/> Code </Link>}/>
          </Dropdown.Item>
          <Dropdown.Item>
            <this.LinkButton trigger={<Link name='link' onClick={() => (this.setState({linkOpen: true}))}>
              <Icon name='linkify'/> Link </Link>}/>
          </Dropdown.Item>
          <Dropdown.Item>
            <this.ImageButton trigger={<Link name='image' onClick={() => (this.setState({imageOpen: true}))}>
              <Icon name='picture'/> Image </Link>}/>
          </Dropdown.Item>
          <Dropdown.Item>
            <this.YoutubeButton trigger={<Link name='youtube' onClick={() => (this.setState({youtubeOpen: true}))}>
              <Icon name='youtube'/> Youtube </Link>}/>
          </Dropdown.Item>
          <this.PostsMenu/>
        </Dropdown.Menu>
      </Dropdown>
  }

  PostsList = () => (this.state.list ?
      this.state.list.sort().reverse().map(date => (<p><Link to={`/tumblelog/${date}`}>{date}</Link></p>))
      : null
  )

  PostsMenu = () => (this.state.list ?
      this.state.list.sort().reverse().map(date => (<Dropdown.Item><Link to={`/tumblelog/${date}`}>{date}</Link></Dropdown.Item>))
      : null
  )

  DesktopLayout = (activeItem, value) => (
    <Responsive getWidth={getWidth} minWidth={Responsive.onlyTablet.minWidth}>
      <Container fluid>
        <Menu>
          <Menu.Item
            name='tumblelog'
            active={activeItem === 'tumblelog'}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            name='edit'
            active={activeItem === 'edit'}
            onClick={this.handleItemClick}
          />
        </Menu>
        {activeItem === 'tumblelog' ?
          <Grid columns={3}>
            <Grid.Row>
              <Grid.Column width={4}></Grid.Column>
              <Grid.Column width={8}>
                <this.ActionsMenu/>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width={4}></Grid.Column>
              <Grid.Column width={8}>
                <MarkdownPreview
                  value={value && value.length > 0 ? value : "This is your personal tumblelog. Add items to your daily post."}
                  className="column comment-preview"/>
              </Grid.Column>
              <Grid.Column width={4}>
                <this.PostsList/>
              </Grid.Column>
            </Grid.Row>
          </Grid>
          : null
        }
        {activeItem === 'edit' ?
          <div>
            <Button onClick={this.publishClicked}>Publish Changes</Button>
            <Button onClick={this.cancelClicked}>Cancel</Button>
            <LiveMarkdownTextarea
              value={value}
              onChange={this.handleChange}
              ref="profileEditor"
              className="row"
              inputClassName="field column"
              previewClassName="column comment-preview"/>
          </div>
          : null
        }
      </Container>
    </Responsive>
  )

  MobileLayout = (activeItem, value) => (
    <Responsive getWidth={getWidth} maxWidth={Responsive.onlyMobile.maxWidth}
    >
      <div>
        <Menu>
          <Menu.Item
            name='tumblelog'
            active={activeItem === 'tumblelog'}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            name='edit'
            active={activeItem === 'edit'}
            onClick={this.handleItemClick}
          />
          <Menu.Menu position='right'>
            <this.ActionsSideMenu/>
          </Menu.Menu>
        </Menu>
        {activeItem === 'tumblelog' ?
          <Container>
            <MarkdownPreview
              value={value && value.length > 0 ? value : "This is your personal tumblelog. Edit to tell other users about yourself."}
              className="column comment-preview"/>
          </Container>
          : null
        }
        {activeItem === 'edit' ?
          <div>
            <Button onClick={this.publishClicked}>Publish Changes</Button>
            <Button onClick={this.cancelClicked}>Cancel</Button>
            <LiveMarkdownTextarea
              value={value}
              onChange={this.handleChange}
              ref="profileEditor"
              className="row"
              inputClassName="field column"
              previewClassName="column comment-preview"/>
          </div>
          : null
        }
      </div>
    </Responsive>
  )

  render() {
    const {activeItem, value} = this.state
    return (
      <div style={{padding:'0px'}}>
        {this.DesktopLayout(activeItem, value)}
        {this.MobileLayout(activeItem, value)}
      </div>
    );
  }
}

function mapState(state) {
  const {authentication} = state;
  const {user} = authentication;
  return {user};
}

const actionCreators = {}

const connectedTumblelogPage = connect(mapState, actionCreators)(TumblelogPage);
export {connectedTumblelogPage as TumblelogPage};
