import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {Card, Container, Header} from 'semantic-ui-react'
import moment from 'moment';

import {userActions} from '../actions';

class UsersPage extends React.Component {
  componentDidMount() {
    this.props.getUsers();
  }

  render() {
    const {users} = this.props;
    return (
      <Container>
        <Header as='h1'>Members</Header>
        {users.loading && <em>Loading members...</em>}
        {users.error && <span className="text-danger">ERROR: {users.error}</span>}
        {users.items &&
        <Card.Group>
          {users.items.map((user, index) =>
            <Card key={user.id}>
              <Card.Content>
                <Card.Header>{user.nickname}</Card.Header>
                <Card.Meta>Member since {moment(user.createdAt).format('MMMM Do YYYY')}</Card.Meta>
              </Card.Content>
              <Card.Content extra>
                <Link to={"/"+user.username}>@{user.username}</Link>
              </Card.Content>
            </Card>
          )}
        </Card.Group>
        }
      </Container>
    );
  }
}

function mapState(state) {
  const {users, authentication} = state;
  const {user} = authentication;
  return {user, users};
}

const actionCreators = {
  getUsers: userActions.getAll,
  deleteUser: userActions.delete
}

const connectedUsersPage = connect(mapState, actionCreators)(UsersPage);
export {connectedUsersPage as UsersPage};
