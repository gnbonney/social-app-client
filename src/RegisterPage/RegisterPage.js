import React from 'react';
import { Link } from 'react-router-dom';
import {Button, Checkbox, Form, Grid, Label, Popup} from 'semantic-ui-react'
import { connect } from 'react-redux';

import { userActions } from '../actions';

class RegisterPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {
                email: '',
                nickname: '',
                username: '',
                password: '',
                organizationType: '',
                officialEmail: ''
            },
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    formatValue(name, value) {
        // don't allow spaces in some fields
        if (['username', 'email', 'officialEmail'].includes(name)) {
            return value.replace(/\s/g, '')
        }
        return value
    }

    handleChange(event, data) {
        const name = data ? data.name : event.target.name;
        const value = data ? data.value : event.target.value
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: this.formatValue(name, value)
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitted: true });
        const { user } = this.state;
        if (user.nickname && user.username && user.password && user.email && (!user.organizationType || user.officialEmail)) {
            this.props.register(user);
        }
    }

    render() {
        const { registering  } = this.props;
        const { user, submitted } = this.state;
        return (
          <Grid columns={3}>
              <Grid.Column width={4}></Grid.Column>
              <Grid.Column width={8}>
                  <h2>Register</h2>
                  <Form name="form" onSubmit={this.handleSubmit}>
                      <div className={'field' + (submitted && !user.nickname ? ' has-error' : '')}>
                          <Popup content="This can be your real name or a handle you wish to go by online; does not have to be unique and can contain spaces" trigger={<label htmlFor="nickname">Nickname (screen name)</label>} />
                          <input type="text" name="nickname" value={user.nickname} onChange={this.handleChange} />
                          {submitted && !user.nickname &&
                              <Label basic color='red' pointing>
                              Nickname is required
                              </Label>
                          }
                      </div>
                      <div className={'field' + (submitted && !user.email ? ' has-error' : '')}>
                          <Popup content="Won't change if you change jobs or schools" trigger={<label htmlFor="email">Personal e-mail address</label>} />
                          <input type="text" name="email" value={user.email} onChange={this.handleChange} />
                          {submitted && !user.email &&
                              <Label basic color='red' pointing>
                              Email address is required
                              </Label>
                          }
                      </div>
                      <div className={'field' + (submitted && !user.organizationType ? ' has-error' : '')}>
                          <Popup content="Once verified, your posts will show up on searches filtered by organization type" trigger={<label htmlFor="organizationType">Orginization Type</label>} />
                          <div>
                              I am...
                          </div>
                          <Form.Field>
                              <Checkbox
                                radio
                                label='a member of the press'
                                name='organizationType'
                                value='press'
                                checked={user.organizationType === 'press'}
                                onChange={this.handleChange}
                              />
                          </Form.Field>
                          <Form.Field>
                              <Checkbox
                                radio
                                label='a government official or employee'
                                name='organizationType'
                                value='government'
                                checked={user.organizationType === 'government'}
                                onChange={this.handleChange}
                              />
                          </Form.Field>
                          <Form.Field>
                              <Checkbox
                                radio
                                label='attending or employed by an educational institution'
                                name='organizationType'
                                value='education'
                                checked={user.organizationType === 'education'}
                                onChange={this.handleChange}
                              />
                          </Form.Field>
                          <Form.Field>
                              <Checkbox
                                radio
                                label='employed by a Fortune 500 company'
                                name='organizationType'
                                value='fortune500'
                                checked={user.organizationType === 'fortune500'}
                                onChange={this.handleChange}
                              />
                          </Form.Field>
                          <Form.Field>
                              <Checkbox
                                radio
                                label='None of the above (or prefer not to say)'
                                name='organizationType'
                                value=''
                                checked={user.organizationType === null}
                                onChange={this.handleChange}
                              />
                          </Form.Field>
                      </div>
                      {user.organizationType ? <div className={'field' + (submitted && !user.officialEmail ? ' has-error' : '')}>
                            <Form.Field>
                                Selected value: <b>{user.organizationType}</b>
                            </Form.Field>
                      <Popup content="Used to verify your organization type selection; Once verified, you will have a special icon by your name when you post" trigger={<label htmlFor="officialEmail">Official Email address</label>} />
                      <input type="text" name="officialEmail" value={user.officialEmail} onChange={this.handleChange} />
                      {submitted && !user.officialEmail &&
                          <Label basic color='red' pointing>
                          Official email address is required if organization type selected
                          </Label>
                      }
                  </div>
                      : null}
                      <div className={'field' + (submitted && !user.username ? ' has-error' : '')}>
                          <Popup content="Must be unique, no spaces allowed" trigger={<label htmlFor="username">Username (login name)</label>} />
                          <input type="text" name="username" value={user.username} onChange={this.handleChange} />
                          {submitted && !user.username &&
                              <Label basic color='red' pointing>
                              Username is required
                              </Label>
                          }
                      </div>
                      <div className={'field' + (submitted && !user.password ? ' has-error' : '')}>
                          <label htmlFor="password">Password</label>
                          <input type="password" name="password" value={user.password} onChange={this.handleChange} />
                          {submitted && !user.password &&
                              <Label basic color='red' pointing>
                              Password is required
                              </Label>
                          }
                      </div>
                      <div className="field">
                          {registering ? <Button loading>Register</Button>
                            : <Button>Register</Button>}
                          <Link to="/login">Cancel</Link>
                      </div>
                  </Form>
              </Grid.Column>
          </Grid>
        );
    }
}

function mapState(state) {
    const { registering } = state.registration;
    return { registering };
}

const actionCreators = {
    register: userActions.register
}

const connectedRegisterPage = connect(mapState, actionCreators)(RegisterPage);
export { connectedRegisterPage as RegisterPage };
