import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {List, Container, Header} from 'semantic-ui-react'
import axios from "axios";
import {authHeader} from "../helpers";

// TODO: in tutorial config.apiUrl was set in webpack.config.js, but that's because they ejected
// FIXME: figure out how to set apiUrl in env
const config = {apiUrl: 'http://192.168.1.129:4000'}

class ChannelsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {list: []};
  }

  componentDidMount() {
    this.getChannelsList()
  }

  getChannelsList() {
    axios.get(`${config.apiUrl}/hashtags`, {headers: authHeader()})
      .then((response) => {
        // handle success
        this.setState({list: response.data})
      })
      .catch((error) => {
        // handle error
        console.log(error)
      })
      .then(() => {
        // always executed
      });
  }

  render() {
    return (
      <Container>
        <Header as='h1'>Channels</Header>
        <List>
          {this.state.list.map((hashtag, index) =>
            <List.Item><Link to={'/hashtag/'+hashtag.substring(1)}>{hashtag}</Link></List.Item>
          )}
        </List>
      </Container>
    );
  }
}

function mapState(state) {
  const {authentication} = state;
  const {user} = authentication;
  return {user};
}

const connectedChannelsPage = connect(mapState)(ChannelsPage);
export {connectedChannelsPage as ChannelsPage};
