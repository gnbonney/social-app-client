import React from 'react';
import {Editor, EditorState} from 'md-draft-js';
import {RichUtils} from 'md-draft-js/src';
import autosize from 'autosize';
import {Button, ButtonGroup, Icon, Popup, Segment} from "semantic-ui-react";
import Alertify from 'alertifyjs';
import classes from "./editor.css";

const commands = require('./commands');

class MarkdownInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {editorState: EditorState.createWithContent(props.value)};
    this.onChange = (editorState) => {
      this.setState({editorState});
      props.onChange(EditorState.getText(editorState));
      autosize(document.querySelectorAll('textarea'));
    }
    this.handleKeyCommand = this.handleKeyCommand.bind(this);
  }

  handleKeyCommand({key}) {
    const newState = RichUtils.applyCommand(this.state.editorState, key);

    if (newState) {
      this.onChange(newState);
    }
  }

  onClickCommand(command) {
    this.onChange(RichUtils.applyCommand(this.state.editorState, command));
  }

  onLinkClick() {
    if (this.state.editorState.selection) {
      Alertify.prompt('Link URL', 'Enter URL', 'URL'
        , (evt, value) => {
          if (value) {
            this.onChange(
              RichUtils.applyCommand(this.state.editorState, 'link', value)
            );
          }
        }
        , () => { Alertify.error('Cancel') });
    }
  }

  onImageClick() {
    Alertify.prompt('Image URL', 'Enter URL', 'URL'
      , (evt, value) => {
        if (value) {
          this.onChange(
            RichUtils.applyCommand(this.state.editorState, 'media', value)
          );
        }
      }
      , () => { Alertify.error('Cancel') });
  }

  render() {
    return (
      <Segment>
        <div>
          {/* TODO: change this from buttongroup to icon menu (https://react.semantic-ui.com/collections/menu/#variations-icons) */}
          <ButtonGroup>
            {commands.map(({command, label, icon}, key) => (
              <Popup content={label} trigger={<Button
                key={key}
                className={
                  this.state.editorState.getCurrentInlineStyle().has(command)
                    ? classes.editorAction.active
                    : classes.editorAction
                }
                onClick={this.onClickCommand.bind(this, command)}
                aria-label="Bold"
              >
                <Icon name={icon || command}/>
              </Button>} />
            ))}
            <Popup content='Link' trigger={<Button
              className={classes.editorAction}
              onClick={this.onLinkClick.bind(this)}
              aria-label="Link"
            >
              <Icon name={"linkify"}/>
            </Button>} />
            <Popup content='Image' trigger={<Button
              className={classes.editorAction}
              onClick={this.onImageClick.bind(this)}
              aria-label="Image"
            >
              <Icon name={"image"}/>
            </Button>} />
          </ButtonGroup>
        </div>
        <Editor
          autoFocus
          // className="editor-textarea"
          editorState={this.state.editorState}
          onKeyCommand={this.handleKeyCommand}
          onChange={this.onChange}
        />
      </Segment>
    );
  }
};

export default MarkdownInput;
