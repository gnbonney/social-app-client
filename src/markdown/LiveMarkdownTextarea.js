import React from 'react';
import {Container, Menu, Segment, Table} from "semantic-ui-react";

// based on LiveMarkdownTextarea from go-e/react-marked-markdown, which is what react-easy-md was based on
import MarkdownInput from "./MarkdownInput";
import MarkdownPreview from "./MarkdownPreview";

export class LiveMarkdownTextarea extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value ? props.value : '', activeItem: 'preview'
    };
    fetch('/Markdown-Cheatsheet.md')
      .then((r) => r.text())
      .then(text  => {
        this.setState({cheatSheet: text})
      })
  }

  handleTextChange = (txt) => {
    this.setState({value: txt});
    this.props.onChange(txt)
  }

  clear() {
    this.setState({value: ''});
  }

  handleItemClick = (e, { name }) => {
    this.setState({ activeItem: name })
  }

  render() {
    const {
      previewClassName
    } = this.props;
    const {activeItem, cheatSheet, value} = this.state;

    return (
      <div>
        <Table fixed>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Markdown</Table.HeaderCell>
              <Table.HeaderCell>
                <Menu pointing>
                  <Menu.Item
                    name='preview'
                    active={activeItem === 'preview'}
                    onClick={this.handleItemClick}
                  />
                  <Menu.Item
                    name='markdown-help'
                    active={activeItem === 'markdown-help'}
                    onClick={this.handleItemClick}
                  />
                </Menu>
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row>
              <Table.Cell verticalAlign={"top"}>
                <MarkdownInput
                  onChange={this.handleTextChange.bind(this)}
                  value={value}
                />
              </Table.Cell>
              <Table.Cell>
                <Segment>
                  <Container>
                    <MarkdownPreview
                      value={activeItem==='preview' ? value : cheatSheet}
                      className={previewClassName}/>
                  </Container>
                </Segment>
              </Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
      </div>
    );
  }
}
