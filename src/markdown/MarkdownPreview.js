import React from "react";
import marked from "marked";
import hljs from "highlight.js";
import ChordSheetJS from 'chordsheetjs';
import PropTypes from "prop-types";
import "highlight.js/styles/github.css"

const escapeMap = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  '"': "&quot;",
  "'": "&#39;"
};

function escapeForHTML(input) {
  return input.replace(/([&<>'"])/g, char => escapeMap[char]);
}

// When render with React for each link inside MarkdownPreview.js
// Different from using regexes for useShortcut
let substitutePrefixes = (href = "https://wikitumble.com") => (set = [["s-", "https://"]]) => {
  const isHrefIncludeAnyPrefix = set.filter(x => href.startsWith(x[0]));

  if(isHrefIncludeAnyPrefix.length === 1) { // === i instead of > 0 to be more precise
    return `${isHrefIncludeAnyPrefix[0][1]}${href.split(isHrefIncludeAnyPrefix[0][0])[1]}`
  } else {
    return href;
  }
}

// replace # hashtags and @ mentions with links
function replaceHashtags(value) {
  const hashRegex = /#[a-zA-Z][0-9a-zA-Z_]+/mg;
  const mentionRegex = /@[a-zA-Z][0-9a-zA-Z_]+/mg;
  return value.replace(hashRegex, value => `<a href='/hashtag/${value.substring(1)}' class='hashtag_link' target='_blank'>${value}</a>`)
    .replace(mentionRegex, value => `<a href='/${value.substring(1)}' class='hashtag_link' target='_blank'>${value}</a>`);
}

function MarkdownPreview({
                           markedOptions,
                           value,
                           className,
                           set,
                           titleMessage,
                         }) {

  let options = {};
  if (markedOptions) {
    options = markedOptions;

    options = {
      gfm: true,
      tables: true,
      breaks: false,
      pedantic: false,
      sanitize: true,
      smartLists: true,
      smartypants: true,
      ...options
    };

    marked.setOptions(options);
  }

  const renderer = new marked.Renderer();

  renderer.code = (code, language) => {
    if (language==='chordpro') {
      // Parse some ChordPro formatted text
      const parser = new ChordSheetJS.ChordProParser();
      const song = parser.parse(code);
      // Render as HTML
      const formatter = new ChordSheetJS.HtmlTableFormatter();
      const html = formatter.format(song)
      return `<div class="ui container">${html}</div>`;
    }

    // Check whether the given language is valid for highlight.js.
    const validLang = !!(language && hljs.getLanguage(language));

    // Highlight only if the language is valid.
    // highlight.js escapes HTML in the code, but we need to escape by ourselves
    // when we don't use it.
    const highlighted = validLang
      ? hljs.highlight(language, code).value
      : escapeForHTML(code);

    // Render the highlighted code with `hljs` class.
    return `<pre><code class="hljs ${language}">${highlighted}</code></pre>`;
  };

  // Remove null title problem when it is undefined by giving default value
  // and you can define shortcuts for <a> in markdown
  renderer.link = (href, title, text) => {
    const lastHref = substitutePrefixes(href)(set);

    return `<a target="_blank" rel="noopener noreferrer" href="${lastHref}" title="${
      title === null ? `${titleMessage} ${lastHref}` : title
    }" >${text}</a>`;
  };

  // replace hashtags and @ mention before converting markup
  const text = replaceHashtags(value);

  // convert to markup
  const html = marked(text || "", { renderer });

  return (
    <div dangerouslySetInnerHTML={{ __html: html }} className={className} />
  );
}

MarkdownPreview.propTypes = {
  value: PropTypes.string,
  className: PropTypes.string,
  markedOptions: PropTypes.object,
  set: PropTypes.array,
  titleMessage: PropTypes.string
};

MarkdownPreview.defaultProps = {
  value: "", // "**This is default value. Write Your own markdown**",
  set: [["s-", "https://"]],
  titleMessage: "Click it will open a new tab at"
};

export default MarkdownPreview;
