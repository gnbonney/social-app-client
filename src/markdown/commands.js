module.exports = [
  {
    command: 'bold',
    icon: 'bold',
    label: 'Bold'
  },
  {
    command: 'italic',
    icon: 'italic',
    label: 'Italic'
  },
  {
    command: 'heading',
    icon: 'heading',
    label: 'Heading'
  },
  {
    command: 'quote',
    icon: 'quote right',
    label: 'Quote'
  },
  {
    command: 'code',
    icon: 'code',
    label: 'Code'
  },
  {
    command: 'ul',
    icon: 'list ul',
    label: 'List'
  },
  {
    command: 'ol',
    icon: 'list ol',
    label: 'Numbered'
  }
];
