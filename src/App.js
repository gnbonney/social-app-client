import React, {Component} from "react";
import {Route, Router, Switch} from "react-router-dom";
import {connect} from 'react-redux';
import {alertActions} from './actions';
import {history} from './helpers/history';
import {PrivateRoute} from './components'
import {ChannelsPage} from "./ChannelsPage";
import {HashtagPage} from './HashtagPage';
import {HomePage} from './HomePage';
import {FeaturesPage} from './FeaturesPage';
import {ProfilePage} from './ProfilePage';
import {TumblelogPage} from './TumblelogPage';
import {UsersPage} from './UsersPage';
import {LoginPage} from './LoginPage';
import {MemberPage} from './MemberPage';
import {RegisterPage} from './RegisterPage';
import {Button, Icon, Menu, Responsive, Sidebar, Visibility} from 'semantic-ui-react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBlog } from '@fortawesome/free-solid-svg-icons'
import moment from 'moment';
import "./App.css";

const getWidth = () => {
  const isSSR = typeof window === 'undefined';

  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth;
};

class DesktopMenu extends Component {
  state = {}

  handleItemClick = (e, {name}) => this.setState({activeItem: name})

  render() {
    const {activeItem} = this.state
    const {children} = this.props;
    return <Responsive getWidth={getWidth} minWidth={Responsive.onlyTablet.minWidth}>
      <Visibility
        once={false}
        onBottomPassed={this.showFixedMenu}
        onBottomPassedReverse={this.hideFixedMenu}
      >
        <Menu>
          <Menu.Item header>WikiTumble.com</Menu.Item>

          <Menu.Item
            name='home'
            active={activeItem === 'home'}
            onClick={this.handleItemClick}
            href={'/'}
            icon='home'
          />

          <Menu.Item
            name='profile'
            active={activeItem === 'profile'}
            onClick={this.handleItemClick}
            href={'/profile'}
          ><Icon name='user'/>My Page</Menu.Item>

          <Menu.Item
            name='tumblelog'
            active={activeItem === 'tumblelog'}
            onClick={this.handleItemClick}
            href={`/tumblelog/${moment().format('YYYY-MM-DD')}`}
          >
            <FontAwesomeIcon icon={faBlog}/>
            <div style={{marginLeft: '0.25em'}}>My Tumblelog</div>
          </Menu.Item>

          <Menu.Item
            name='users'
            active={activeItem === 'users'}
            onClick={this.handleItemClick}
            href={'/users'}
          ><Icon name='users'/>Members</Menu.Item>

          <Menu.Item
            name='channels'
            active={activeItem === 'channels'}
            onClick={this.handleItemClick}
            href={'/channels'}
            icon='hashtag'
          />

          <Menu.Menu position='right'>
            <Menu.Item
              name='login'
              active={activeItem === 'login'}
              onClick={this.handleItemClick}
              href={'/login'}
            />

            <Menu.Item
              name='signup'
              active={activeItem === 'signup'}
              onClick={this.handleItemClick}
              href={'/register'}
            >
              Sign Up
            </Menu.Item>
          </Menu.Menu>
        </Menu>
        {children}
      </Visibility>
    </Responsive>
  }
}

class MobileMenu extends Component {
  state = {}

  handleItemClick = (e, {name}) => this.setState({activeItem: name})

  render() {
    const {activeItem} = this.state
    const {children} = this.props;
    return <Responsive
      as={Sidebar.Pushable}
      getWidth={getWidth}
      maxWidth={Responsive.onlyMobile.maxWidth}
    >
      <Visibility
        once={false}
        onBottomPassed={this.showFixedMenu}
        onBottomPassedReverse={this.hideFixedMenu}
      >
        <Menu>
          <Menu.Item
            name=''
            active={activeItem === 'home'}
            onClick={this.handleItemClick}
            href={'/'}
            icon='home'
          />

          <Menu.Item
            name=''
            active={activeItem === 'profile'}
            onClick={this.handleItemClick}
            href={'/profile'}
            icon='user'
          />

          <Menu.Item
            name='tumblelog'
            active={activeItem === 'tumblelog'}
            onClick={this.handleItemClick}
            href={`/tumblelog/${moment().format('YYYY-MM-DD')}`}
          >
            <FontAwesomeIcon icon={faBlog}/>
          </Menu.Item>

          <Menu.Item
            name=''
            active={activeItem === 'users'}
            onClick={this.handleItemClick}
            href={'/users'}
            icon='users'
          />

          <Menu.Item
            name=''
            active={activeItem === 'channels'}
            onClick={this.handleItemClick}
            href={'/channels'}
            icon='hashtag'
          />

          <Menu.Menu position='right'>
            <Menu.Item>
            <Button compact as="a" href={'/login'}>
              Log in
            </Button>
            <Button compact as="a" href={'/register'} style={{marginLeft: '0.5em'}}>
              Sign Up
            </Button>
            </Menu.Item>
          </Menu.Menu>

        </Menu>
        {children}
      </Visibility>
    </Responsive>
  }
}

class App extends Component {
  constructor(props) {
    super(props);

    history.listen((location, action) => {
      // clear alert on location change
      this.props.clearAlerts();
    });
  }

  render() {

    return (
      <div>
        <DesktopMenu/>
        <MobileMenu/>
        <Router history={history}>
          <Switch>
            <Route exact path="/" component={HomePage}/>
            <PrivateRoute path="/profile" component={ProfilePage}/>
            <PrivateRoute path="/tumblelog/:date" component={TumblelogPage}/>
            <Route path="/features" component={FeaturesPage}/>
            <Route path="/register" component={RegisterPage}/>
            <Route path="/login" component={LoginPage}/>
            <Route path="/channels" component={ChannelsPage}/>
            <Route path="/hashtag/:hashtag" component={HashtagPage}/>
            <Route path="/users" component={UsersPage}/>
            <Route path="/:handle" component={MemberPage}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

function mapState(state) {
  const {alert} = state;
  return {alert};
}

const actionCreators = {
  clearAlerts: alertActions.clear
};

const connectedApp = connect(mapState, actionCreators)(App);
export {connectedApp as App};
