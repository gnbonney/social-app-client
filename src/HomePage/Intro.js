import {Button, Grid, Header, Image} from "semantic-ui-react";
import React from "react";

const Intro = () => {
  return <Grid container stackable verticalAlign="middle">
    <Grid.Row>
      <Grid.Column width={8}>
        <Header as="h3" style={{fontSize: '2em'}}>
          Personal tumblelogs (microblogs)
        </Header>
        <p style={{fontSize: '1.33em'}}>
          <p>
            A <b>tumblelog</b> is a stream of consciousness blog that supports short-form mixed media content.
            Share your ongoing thoughts and ideas in your own personal tumblelog.
            Easily add thoughts, quotes, song lyrics, links to articles and videos with
            preview thumbnail and description, images (externally hosted), and code blocks.
            Use your about page to describe yourself and/or get your point across.
            Edit your entries using <b>wiki</b> markup.
          </p>
        </p>
        <Header as="h3" style={{fontSize: '2em'}}>
          Your Feed
        </Header>
        <p style={{fontSize: '1.33em'}}>
          <p>
            Other members can mention you by using @nickname and mentions show up in your feed.
            Your feed can easily be filtered so you only see mentions from communities such as members of the press.
          </p>
        </p>
        <Header as="h3" style={{fontSize: '2em'}}>
          Groups
        </Header>
        <p style={{fontSize: '1.33em'}}>
          Create your own groups simply by using a hashtag (#groupname) in your personal about page or tumblelog.
          Members may then edit the group's about page and contribute to the group tumblelog by
          using the group hashtag in their about page or tumblelog.
        </p>
        <Header as="h3" style={{fontSize: '2em'}}>
          Search and follow
        </Header>
        <p style={{fontSize: '1.33em'}}>
          Search and follow members and groups.
          When you follow a tumblelog its posts will be shown in your feed.
        </p>
      </Grid.Column>
      <Grid.Column floated="right" width={6}>
        {/* <Image bordered rounded size='large' src='/images/wireframe/white-image.png' /> */}
        <Image bordered rounded size="large" src="/images/screenshots/Screen Shot 2020-05-01 at 7.57.35 PM.png"/>
        Anarchaia was the original tumblelog created by Leah Neukirchen.
      </Grid.Column>
    </Grid.Row>
    <Grid.Row>
      <Grid.Column textAlign="center">
        <Button size="huge" as="a" href={'/register'}>I've heard enough. Let's get started!</Button>
      </Grid.Column>
    </Grid.Row>
  </Grid>
}

export default Intro
