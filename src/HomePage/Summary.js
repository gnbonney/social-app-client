import {Button, Container, Divider, Header, Icon, List} from "semantic-ui-react";
import React from "react";

const Summary = () => {
  return <Container text>
    <Header as="h3" style={{ fontSize: '2em' }}>
      Social Media Solutions
    </Header>
    <p style={{ fontSize: '1.33em' }}>
      <div>
        Established social media platforms have little incentive to
        fix their problems. They'd rather convince us that we are the problem.
        They exploit their users and invade our privacy for profit while maintaining features
        that accelerate and amplify fake news, public shaming, negativity
        and disharmony in society.
      </div>
      <p>
        We need a fresh start away from all the negativity. Together we can find real solutions.
      </p>
    </p>
    <Button size="huge" as="a" href={'/features'}>
      Learn more
    </Button>
  </Container>
}

export default Summary
