import React from 'react';
import { connect } from 'react-redux';

import { userActions } from '../actions';
import {Button, Container, Header, Icon, Responsive, Segment, Sidebar} from "semantic-ui-react";
import Intro from "./Intro";
import Summary from "./Summary";
import Footer from "./Footer";

const Heading = (mobile) => {
  return <Container text>
    <Header
      as="h1"
      content="wikitumble"
      inverted
      style={{
        fontSize: mobile ? '2em' : '4em',
        fontWeight: 'normal',
        marginBottom: 0,
        marginTop: mobile ? '1.5em' : '3em',
      }}
    />
    <div>
      Together we are greater than the sum of our parts.
    </div>
    <Header
      as="h2"
      content="wiki + tumblelogs = wikitumble"
      inverted
      style={{
        fontSize: mobile ? '1.5em' : '1.7em',
        fontWeight: 'normal',
        marginTop: mobile ? '0.5em' : '1.5em',
      }}
    />
    <Button primary size="huge" href={'/register'}>
      Express Yourself
      <Icon name="right arrow" />
    </Button>
  </Container>
}

const getWidth = () => {
  const isSSR = typeof window === 'undefined';

  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth;
};

class HomePage extends React.Component {
  render() {
    return (
      <div>
        <Segment
          inverted
          textAlign="center"
          style={{ minHeight: 700, padding: '1em 0em' }}
          vertical
        >
          <Responsive getWidth={getWidth} minWidth={Responsive.onlyTablet.minWidth}>
            {Heading(false)}
          </Responsive>
          <Responsive
            as={Sidebar.Pushable}
            getWidth={getWidth}
            maxWidth={Responsive.onlyMobile.maxWidth}
          >
            {Heading(true)}
          </Responsive>
        </Segment>
        <Segment style={{padding: '8em 0em'}} vertical>
          <Intro/>
        </Segment>
        <Segment style={{padding: '8em 0em'}} vertical>
          <Summary/>
        </Segment>
        <Segment inverted vertical style={{padding: '5em 0em'}}>
          <Footer/>
        </Segment>      </div>
    );
  }
}

function mapState(state) {
  const { users, authentication } = state;
  const { user } = authentication;
  return { user, users };
}

const actionCreators = {
  getUsers: userActions.getAll,
  deleteUser: userActions.delete
}

const connectedHomePage = connect(mapState, actionCreators)(HomePage);
export { connectedHomePage as HomePage };
