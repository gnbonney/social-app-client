import React from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import {LiveMarkdownTextarea} from '../markdown';
import {Button, Container, Grid, Menu, Responsive} from 'semantic-ui-react'
import {authHeader} from '../helpers/auth-header';
import MarkdownPreview from "../markdown/MarkdownPreview";

const getWidth = () => {
  const isSSR = typeof window === 'undefined';

  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth;
};

// TODO: in tutorial config.apiUrl was set in webpack.config.js, but that's because they ejected
// FIXME: figure out how to set apiUrl in env
const config = {apiUrl: 'http://192.168.1.129:4000'}

class ProfilePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {activeItem: 'page'};
  }

  componentDidMount() {
    const {user} = this.props
    axios.get(`${config.apiUrl}/pages/${user.username}`, {headers: authHeader()})
      .then((response) => {
        // handle success
        this.setState({value: response.data.markdown})
      })
      .catch((error) => {
        // handle error
        console.log(error)
      })
      .then(() => {
        // always executed
      });
  }

  handleItemClick = (e, {name}) => {
    if (name === 'edit') {
      this.setState({temp: this.state.value})
    }
    this.setState({activeItem: name})
  }

  cancelClicked = (event) => {
    this.setState({value: this.state.temp, activeItem: 'page'})
    // cancel the event to prevent form submit
    event.preventDefault();
  }

  publishClicked = (event) => {
    const {user} = this.props
    const {value} = this.state
    axios.post(`${config.apiUrl}/pages/add`, {
      name: user.username,
      markdown: value,
      createdBy: user.username
    }, {headers: authHeader()})
      .then((response) => {
        // handle success
        this.setState({activeItem: 'page'})
      })
      .catch((error) => {
        // handle error
        console.log(error);
        // TODO: display some error
      })
      .then(() => {
        // always executed
      });
    // cancel the event to prevent the default submit behavior
    event.preventDefault();
  }

  handleChange = (txt) => {
    // this.setState({value: event.target.value});
    this.setState({value: txt})
  }

  DesktopLayout = (activeItem, value) => (
    <Responsive getWidth={getWidth} minWidth={Responsive.onlyTablet.minWidth}>
      <div>
        <Menu>
          <Menu.Item
            name='page'
            active={activeItem === 'page'}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            name='edit'
            active={activeItem === 'edit'}
            onClick={this.handleItemClick}
          />
        </Menu>
        {activeItem === 'page' ?
          <Grid columns={3}>
            <Grid.Row>
              <Grid.Column width={4}></Grid.Column>
              <Grid.Column width={8}>
                <MarkdownPreview
                  value={value && value.length > 0 ? value : "This is your personal page. Edit to tell other users about yourself."}
                  className="column comment-preview"/>
              </Grid.Column>
              <Grid.Column width={4}></Grid.Column>
            </Grid.Row>
          </Grid>
          : null
        }
        {activeItem === 'edit' ?
          <div>
            <Button onClick={this.publishClicked}>Publish Changes</Button>
            <Button onClick={this.cancelClicked}>Cancel</Button>
            <LiveMarkdownTextarea
              value={value}
              onChange={this.handleChange}
              ref="profileEditor"
              className="row"
              inputClassName="field column"
              previewClassName="column comment-preview"/>
          </div>
          : null
        }
      </div>
    </Responsive>
  )

  MobileLayout = (activeItem, value) => (
    <Responsive getWidth={getWidth} maxWidth={Responsive.onlyMobile.maxWidth}>
      <div>
        <Menu>
          <Menu.Item
            name='page'
            active={activeItem === 'page'}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            name='edit'
            active={activeItem === 'edit'}
            onClick={this.handleItemClick}
          />
        </Menu>
        {activeItem === 'page' ?
          <Container>
            <MarkdownPreview
              value={value && value.length > 0 ? value : "This is your personal page. Edit to tell other users about yourself."}
              className="column comment-preview"/>
          </Container>
          : null
        }
        {activeItem === 'edit' ?
          <div>
            <Button onClick={this.publishClicked}>Publish Changes</Button>
            <Button onClick={this.cancelClicked}>Cancel</Button>
            <LiveMarkdownTextarea
              value={value}
              onChange={this.handleChange}
              ref="profileEditor"
              className="row"
              inputClassName="field column"
              previewClassName="column comment-preview"/>
          </div>
          : null
        }
      </div>
    </Responsive>
  )

  render() {
    const {activeItem, value} = this.state;
    return (
      <div style={{padding:'0px'}}>
        {this.DesktopLayout(activeItem, value)}
        {this.MobileLayout(activeItem, value)}
      </div>
    );
  }
}

function mapState(state) {
  const {authentication} = state;
  const {user} = authentication;
  return {user};
}

const actionCreators = {}

const connectedProfilePage = connect(mapState, actionCreators)(ProfilePage);
export {connectedProfilePage as ProfilePage};
